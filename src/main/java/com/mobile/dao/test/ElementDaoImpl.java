package com.mobile.dao.test;


import com.mobile.util.test.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;


/**
 * Created by zgurskaya on 19.04.2016.
 */

public class ElementDaoImpl<E> implements ElementDao<E> {

    private Class<E> elementClass;

    public ElementDaoImpl(Class<E> elementClass){
        this.elementClass = elementClass;
    }


    @Transactional(propagation = Propagation.MANDATORY)
    public void addElement(E el) {
        Session session = null;

        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx= session.beginTransaction();
            session.save(el);
            tx.commit();
        } catch (Exception e ){
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public void updateElement(E el) {
        Session session = null;

        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.update(el);
            transaction.commit();
        } catch (Exception e ){
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public E getElementByID(Long elId) {
        Session session = null;
        E el = null;
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            el = (E) session.get(elementClass, elId);
            transaction.commit();
        } catch (Exception e ){
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return el;
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public List<E> getAllElements() {
        Session session = null;
        List<E> els = null;
        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            /*Query q = session.createQuery("from com.mobile.model.test.Test1 ");
            els = q.list();*/
            els = session.createCriteria(elementClass).list();
            transaction.commit();

        }catch (Exception e){
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return els;
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public void deleteElement(E el) {
        Session session = null;

        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx= session.beginTransaction();
            session.delete(el);
            tx.commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

}
