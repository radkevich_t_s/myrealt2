package com.mobile.dao.test;

import com.mobile.model.test.Test2;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Created by zgurskaya on 25.04.2016.
 */

@Repository
public class Test2Dao  extends ElementDaoImpl<Test2> {

    public Test2Dao() {
        super(Test2.class);
    }


    public void addElement(Test2 el) {
        super.addElement(el);
    }

    public void updateElement(Test2 el) {
        super.updateElement(el);
    }

    public Test2 getElementByID(Long elId) {
        return super.getElementByID(elId);
    }

    public List<Test2> getAllElements() {
        return super.getAllElements();
    }

    public void deleteElement(Test2 el) {
        super.deleteElement(el);
    }

}
