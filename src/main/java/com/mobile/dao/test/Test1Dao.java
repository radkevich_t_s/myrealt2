package com.mobile.dao.test;

import com.mobile.model.test.Test1;
import com.mobile.util.test.HibernateUtil;
import oracle.jdbc.OracleTypes;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.service.jdbc.connections.spi.ConnectionProvider;
import org.springframework.stereotype.Repository;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zgurskaya on 19.04.2016.
 */

@Repository

public class Test1Dao extends ElementDaoImpl<Test1> {

    public Test1Dao() {
        super(Test1.class);
    }


    public void addElement(Test1 el) {
        super.addElement(el);
    }

    public void updateElement(Test1 el) {
        super.updateElement(el);
    }

    public Test1 getElementByID(Long elId) { return super.getElementByID(elId);}

    public List<Test1> getAllElements() {
        return super.getAllElements();
    }

    public void deleteElement(Test1 el) {
        super.deleteElement(el);
    }


    public String getStringData(){
        String testStr = "";
        Session session = null;

        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
            ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
            Connection connection = connectionProvider.getConnection();
            CallableStatement call = connection.prepareCall("{ ? = call TEST_GET_DATA_VALUE() }");
            call.registerOutParameter( 1, OracleTypes.VARCHAR);
            call.execute();
            testStr  = (String) call.getObject(1);
            connection.close();
            call.close();
        }catch (Exception e){
            e.printStackTrace();
        } finally {

            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return testStr;
    }

    public List<Test1> getRefCursorData(){
        List result = new ArrayList();
        Session session = null;

        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
            ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
            Connection connection = connectionProvider.getConnection();


            CallableStatement call = connection.prepareCall("{ ? = call TEST_GET_DATA_CURSOR() }");
            call.registerOutParameter( 1, OracleTypes.CURSOR);
            call.execute();
            ResultSet results = (ResultSet) call.getObject(1);

            while (results.next()) {
                Test1 obj = new Test1();
                obj.setId(results.getLong("ID"));
                obj.setDataD(results.getDate("DATA_D"));
                obj.setDataF(results.getFloat("DATA_F"));
                obj.setDataN(results.getLong("DATA_N"));
                obj.setDataV(results.getString("DATA_V"));
                result.add(obj);
                // do something with the results...
            }
            results.close();
            connection.close();
            call.close();

        }catch (Exception e){
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return result;
    }


    public Test1 getRefCursorObject(){
        Test1 obj = new Test1();
        Session session = null;

        try {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
            ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
            Connection connection = connectionProvider.getConnection();

            CallableStatement call = connection.prepareCall("{ ? = call TEST_GET_DATA_CURSOR(?) }");
            call.registerOutParameter(1, OracleTypes.CURSOR);
            call.setLong(2, 1);
            call.execute();
            ResultSet results = (ResultSet) call.getObject(1);

            while (results.next()) {
                obj.setId(results.getLong("ID"));
                obj.setDataD(results.getDate("DATA_D"));
                obj.setDataF(results.getFloat("DATA_F"));
                obj.setDataN(results.getLong("DATA_N"));
                obj.setDataV(results.getString("DATA_V"));
            }
            results.close();
            connection.close();
            call.close();

        }catch (Exception e){
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return obj;

    }
}
