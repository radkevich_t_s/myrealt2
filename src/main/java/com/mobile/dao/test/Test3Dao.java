package com.mobile.dao.test;


import com.mobile.model.test.Test3;
import java.util.List;

/**
 * Created by zgurskaya on 25.04.2016.
 */
public class Test3Dao extends ElementDaoImpl<Test3> {

    public Test3Dao() {
        super(Test3.class);
    }


    public void addElement(Test3 el) {
        super.addElement(el);
    }

    public void updateElement(Test3 el) {
        super.updateElement(el);
    }

    public Test3 getElementByID(Long elId) {
        return super.getElementByID(elId);
    }

    public List<Test3> getAllElements() {
        return super.getAllElements();
    }

    public void deleteElement(Test3 el) {
        super.deleteElement(el);
    }

}
