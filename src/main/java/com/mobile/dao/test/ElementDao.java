package com.mobile.dao.test;


import java.util.List;

/**
 * Created by zgurskaya on 19.04.2016.
 */
public interface ElementDao<E> {

    public void addElement(E el);
    public void updateElement(E el);
    public E getElementByID(Long elId);
    public List<E> getAllElements();
    public void deleteElement(E el);
}
