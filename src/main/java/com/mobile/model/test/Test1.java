package com.mobile.model.test;

/**
 * Created by zgurskaya on 18.04.2016.
 */

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "TEST1", schema = "CENTRAL")
public class Test1 {

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "DATA_N")
    private Long dataN;

    @Column(name = "DATA_V", length = 10)
    private String dataV;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATA_D", length = 100)
    private Date dataD;

    @Column(name = "DATA_F", precision = 6)
    private Float dataF;

    //todo - исправить на ленивую загрузку

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "test1", cascade = CascadeType.ALL)
    private Set<Test2> records = new HashSet<Test2>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "TEST_4", joinColumns = {@JoinColumn(name = "TEST_1_ID", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "TEST_4_ID", nullable = false)})
    private Set<Test3> tests = new HashSet<Test3>();


    public Test1() {
    }

    public Test1(int i) {
        this.dataN = 1l;
        this.dataV = "HELLO OOYEEEEEEEEEEEEE))";
        this.dataD = new Date();
        this.dataF = 123f;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDataN() {
        return dataN;
    }

    public void setDataN(Long dataN) {
        this.dataN = dataN;
    }

    public String getDataV() {
        return dataV;
    }

    public void setDataV(String dataV) {
        this.dataV = dataV;
    }

    public Date getDataD() {
        return dataD;
    }

    public void setDataD(Date dataD) {
        this.dataD = dataD;
    }

    public Float getDataF() {
        return dataF;
    }

    public void setDataF(Float dataF) {
        this.dataF = dataF;
    }

    public Set<Test2> getRecords() {
        return records;
    }

    public void setRecords(Set<Test2> records) {
        this.records = records;
    }

    public Set<Test3> getTests() {
        return tests;
    }

    public void setTests(Set<Test3> tests) {
        this.tests = tests;
    }
}
