package com.mobile.model.test;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by zgurskaya on 19.04.2016.
 */

@Entity
@Table(schema = "CENTRAL", name="TEST_3")
public class Test3 {

    @Id
    @Column(name="ID")
    private Long id;

    @Column(name = "NAME", length = 100)
    private String name;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "tests")
    private Set<Test1> others = new HashSet<Test1>();

    public Test3() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Test1> getOthers() {
        return others;
    }

    public void setOthers(Set<Test1> others) {
        this.others = others;
    }
}
