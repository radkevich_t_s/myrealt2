package com.mobile.model.test;

import javax.persistence.*;

/**
 * Created by zgurskaya on 18.04.2016.
 */

@Entity
@Table(schema = "CENTRAL", name="TEST_2")
public class Test2 {

    @Id
    @Column(name="ID")
    private Long id;

    @Column(name = "NAME", length = 100)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "TEST_1_ID", nullable = false)
    private Test1 test1 = new Test1();

    public Test2() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Test1 getTest1() {
        return test1;
    }

    public void setTest1(Test1 test1) {
        this.test1 = test1;
    }
}
