package com.mobile.services.test;

/**
 * Created by Shcherbina on 08.04.2016.
 */

import com.mobile.model.test.User;

import java.util.List;




public interface UserService {

    User findById(long id);

    User findByName(String name);

    void saveUser(User user);

    void updateUser(User user);

    void deleteUserById(long id);

    List<User> findAllUsers();

    void deleteAllUsers();

    public boolean isUserExist(User user);

}