package com.mobile.services.test;

import com.mobile.dao.test.Test3Dao;
import com.mobile.model.test.Test3;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by zgurskaya on 25.04.2016.
 */
@Service("service3")

public class MyTestServiceObj3 {
    Test3Dao test3dao = new Test3Dao();

    @Transactional(value = "transactionManager",propagation = Propagation.REQUIRED/*, rollbackFor = Exception.class*/)
    public List<Test3> getAllData() {
        return test3dao.getAllElements();
    }

    @Transactional(value = "transactionManager",propagation = Propagation.REQUIRED/*, rollbackFor = Exception.class*/)
    public void addElement(Test3 obj){ test3dao.addElement(obj); }

    @Transactional(value = "transactionManager",propagation = Propagation.REQUIRED/*, rollbackFor = Exception.class*/)
    public void updateElement(Test3 obj){ test3dao.updateElement(obj); }

    @Transactional(value = "transactionManager",propagation = Propagation.REQUIRED/*, rollbackFor = Exception.class*/)
    public void deleteElement(Test3 obj){ test3dao.deleteElement(obj); }

    public Test3 findById(long id){
        return test3dao.getElementByID(id);
    }


}
