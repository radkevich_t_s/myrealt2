package com.mobile.services.test;

import com.mobile.dao.test.Test1Dao;
import com.mobile.model.test.Test1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by zgurskaya on 19.04.2016.
 */
@Service("service1")
public class MyTestService{

    Test1Dao test1dao = new Test1Dao();

    @Transactional(value = "transactionManager",propagation = Propagation.REQUIRED/*, rollbackFor = Exception.class*/)
    public List<Test1> getAllData() {
        return test1dao.getAllElements();
    }

    @Transactional(value = "transactionManager",propagation = Propagation.REQUIRED/*, rollbackFor = Exception.class*/)
    public void addElement(Test1 obj){ test1dao.addElement(obj); }

    @Transactional(value = "transactionManager",propagation = Propagation.REQUIRED/*, rollbackFor = Exception.class*/)
    public void updateElement(Test1 obj){ test1dao.updateElement(obj); }

    @Transactional(value = "transactionManager",propagation = Propagation.REQUIRED/*, rollbackFor = Exception.class*/)
    public void deleteElement(Test1 obj){ test1dao.deleteElement(obj); }

    @Transactional(value = "transactionManager",propagation = Propagation.REQUIRED/*, rollbackFor = Exception.class*/)
    public Test1 findById(long id){
        return test1dao.getElementByID(id);
    }

    public String getStringData() { return test1dao.getStringData(); }

    public List<Test1> getRefCursor() { return test1dao.getRefCursorData(); }

    public Test1 getRefCursorObject(){ return test1dao.getRefCursorObject(); }

}
