package com.mobile.services.test;

import com.mobile.dao.test.Test2Dao;
import com.mobile.model.test.Test2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by zgurskaya on 25.04.2016.
 */

@Service("service2")
public class MyTestServiceObj2 {
    Test2Dao test2dao = new Test2Dao();

    @Transactional(value = "transactionManager",propagation = Propagation.REQUIRED/*, rollbackFor = Exception.class*/)
    public List<Test2> getAllData() {
        return test2dao.getAllElements();
    }

    @Transactional(value = "transactionManager",propagation = Propagation.REQUIRED/*, rollbackFor = Exception.class*/)
    public void addElement(Test2 obj){ test2dao.addElement(obj); }

    @Transactional(value = "transactionManager",propagation = Propagation.REQUIRED/*, rollbackFor = Exception.class*/)
    public void updateElement(Test2 obj){ test2dao.updateElement(obj); }

    @Transactional(value = "transactionManager",propagation = Propagation.REQUIRED/*, rollbackFor = Exception.class*/)
    public void deleteElement(Test2 obj){ test2dao.deleteElement(obj); }

    public Test2 findById(long id){
        return test2dao.getElementByID(id);
    }

}
