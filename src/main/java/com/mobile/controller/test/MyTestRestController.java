package com.mobile.controller.test;

import com.mobile.model.test.Test1;
import com.mobile.services.test.MyTestService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * Created by zgurskaya on 19.04.2016.
 */


@RestController
@RequestMapping("/zna")
public class MyTestRestController {

    MyTestService myTestService = new MyTestService();

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public ResponseEntity<List<Test1>> listAllUsers() {
        System.out.print("Thi is controller");
        List<Test1> test1List = myTestService.getAllData();
        if(test1List.isEmpty()){
            return new ResponseEntity<List<Test1>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Test1>>(test1List, HttpStatus.OK);

        /*return new ResponseEntity<List<Test1>>( myTestService.getAllData(), HttpStatus.OK);*/
    }

    @RequestMapping(value = "/test/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Test1> getTest (@PathVariable("id") long id) {
        Test1 test = (Test1) myTestService.findById(id);
        if (test == null) {
            return new ResponseEntity<Test1>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Test1>(test, HttpStatus.OK);
    }

}
