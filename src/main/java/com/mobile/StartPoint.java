package com.mobile;

import com.mobile.model.test.Test1;
import com.mobile.model.test.Test2;
import com.mobile.model.test.Test3;
import com.mobile.services.test.MyTestService;
import com.mobile.services.test.MyTestServiceObj2;
import com.mobile.services.test.MyTestServiceObj3;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by zgurskaya on 20.04.2016.
 */
public class StartPoint {

    public static void main (String[] args) {

        MyTestService myTestService = new MyTestService();
        MyTestServiceObj2 obj2Service = new MyTestServiceObj2();
        MyTestServiceObj3 service3 = new MyTestServiceObj3();
        //String data = myTestService.getStringData();
        //List list = myTestService.getRefCursor();
        //Test1 obj = (Test1) myTestService.getRefCursorObject();


        /*Test1 obj = new Test1();
        obj.setId(5L);
        obj.setDataN(11L);
        obj.setDataV("many");
        obj.setDataD(null);
        obj.setDataF(1978.0f);

        myTestService.addElement(obj);
*/
        Test1 obj = myTestService.findById(5L);
        Test1 obj2 = myTestService.findById(1L);

        Set others = new HashSet();
        others.add(obj);
        others.add(obj2);

        /*Test2 test = new Test2();
        test.setId(4L);
        test.setName("te1");
        test.setTest1(obj);

        Test2 test2 = new Test2();
        test2.setId(5L);
        test2.setName("tes");
        test2.setTest1(obj);

        Set<Test2> records = new HashSet<Test2>();
        records.add(test);
        records.add(test2);
        obj.setRecords(records);
        */

        Test3 t1 = new Test3();
        t1.setId(1L);
        t1.setName("t1");
        t1.setOthers(others);

        Test3 t2 = new Test3();
        t2.setId(2L);
        t2.setName("t2");
        t2.setOthers(others);

        Set<Test3> tests = new HashSet<Test3>();
        tests.add(t1);
        tests.add(t2);
/*
        obj.setTests(tests);
        obj2.setTests(tests);

        myTestService.updateElement(obj);
        myTestService.updateElement(obj2);*/

        //obj2Service.addElement(test);
        //obj2Service.addElement(test2);

        //service3.addElement(t1);
        //service3.addElement(t2);

        obj2.setTests(tests);
        myTestService.updateElement(obj2);

        List <Test1> list =  myTestService.getAllData();
        for(Test1 o : list){
            System.out.print(o.getDataV());
        }
        //myTestService.addElement(obj);
        //myTestService.updateElement(obj);
        //myTestService.deleteElement(obj);
    }
}
